package genesiseusebio.pm4b.facci.com.practicacinco;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogLogin = new Dialog(MainActivity.this);
                dialogLogin.setContentView(R.layout.dlg_log);

                Button botonAutenticar = (Button)dialogLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario =(EditText)dialogLogin.findViewById(R.id.txtUser);
                final EditText cajaClave =(EditText)dialogLogin.findViewById(R.id.txPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, cajaUsuario.getText().toString()+" "+ cajaClave.getText().toString()
                                .toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogLogin.show();
                break;
            case R.id.opcionRegistar:
                break;
        }
        return true;
    }
}
